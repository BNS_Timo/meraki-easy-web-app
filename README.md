# Meraki Easy Web App

This web app provides very limited access to the Meraki Dashboard.  
For example, an on-site technician can use it to perform an RMA exchange,  
perform a ping from e.g. an MX or MS, add new devices, etc.  

## Welcome Page
![](README_Files/Meraki Easy Web App1.png)

## Claim a new Device
![](README_Files/Meraki Easy Web App2.png)

## Clone the configuration of an existing device (for example for RMA)
![](README_Files/Meraki Easy Web App3.png)

## Troubleshooting tools
![](README_Files/Meraki Easy Web App4.png)

## Link to the Meraki Support Site
![](README_Files/Meraki Easy Web App5.png)