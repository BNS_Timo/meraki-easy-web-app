from django.apps import AppConfig


class MerakieasywebappConfig(AppConfig):

    default_auto_field = "django.db.models.BigAutoField"
    name = "merakiEasyWebApp"
