import os
from django import forms
from dotenv import load_dotenv

from merakiEasyWebApp.merakiAPI.merakiAPI import merakiAPI


class BlinkForm(forms.Form):

    ORGANIZATIONS = []
    NETWORKS = []
    DEVICES = []

    load_dotenv()
    meraki = merakiAPI(os.getenv('API_KEY'))
    organizations = meraki.getOrganizations()
    networks = []
    devices = []

    for organzation in organizations:
        networks.append(meraki.getNetworks(organzation['id']))

    for network in networks[0]:
        deviceList = meraki.getDevices(network['id'])
        for device in deviceList:
            devices.append(device)

    for organization in organizations:
        ORGANIZATIONS.append((str(organization['id']), str(organization['name'])))

    for network in networks[0]:
        NETWORKS.append((str(network['id']), str(network['name'])))

    for device in devices:
        DEVICES.append((str(device['serial']), str(device['name'])))

    device = forms.ChoiceField(label='Device', choices=DEVICES)