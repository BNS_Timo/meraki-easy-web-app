import os
from django import forms
from dotenv import load_dotenv
from merakiEasyWebApp.merakiAPI.merakiAPI import merakiAPI


class ClaimForm(forms.Form):

    ORGANIZATIONS = []
    NETWORKS = []

    load_dotenv()
    meraki = merakiAPI(os.getenv('API_KEY'))
    organizations = meraki.getOrganizations()
    networks = []

    for organzation in organizations:
        networks.append(meraki.getNetworks(organzation['id']))

    for organization in organizations:
        ORGANIZATIONS.append((str(organization['id']), str(organization['name'])))

    for network in networks[0]:
        NETWORKS.append((str(network['id']), str(network['name'])))

    organizations = forms.ChoiceField(label='Organizations', choices=ORGANIZATIONS)
    networks = forms.ChoiceField(label='Networks', choices=NETWORKS)
    serial_or_order_number = forms.CharField(label='Serial- or Ordernumber')