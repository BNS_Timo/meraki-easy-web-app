from django import forms

class LoginForm(forms.Form):

	user_field = forms.CharField(label='Username')
	pass_field = forms.CharField(label='Password', widget=forms.PasswordInput)
