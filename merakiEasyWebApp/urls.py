from django.urls import path

from merakiEasyWebApp.views.claimView import ClaimView
from merakiEasyWebApp.views.cloneView import CloneView
from merakiEasyWebApp.views.indexView import IndexView
from merakiEasyWebApp.views.supportView import SupportView
from merakiEasyWebApp.views.troubleView import TroubleView
from merakiEasyWebApp.views.loginView import LoginView
from django.contrib.auth.views import LogoutView

urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    path('claim', ClaimView.as_view(), name='claim'),
    path('clone', CloneView.as_view(), name='clone'),
    path('trouble', TroubleView.as_view(), name='trouble'),
    path('support', SupportView.as_view(), name='support'),
    path('login', LoginView.as_view(), name='login'),
    path('logout', LogoutView.as_view(), name='logout'),
]
