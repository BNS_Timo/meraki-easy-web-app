from django.contrib.auth.models import User
from django.db import models


class CustomUser(models.Model):

    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE
    )

    api_key = models.CharField(
        max_length=50,
    )
