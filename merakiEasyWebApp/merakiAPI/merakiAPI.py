import meraki


class merakiAPI():

    def __init__(self, apikey):
        self.API_KEY = apikey
        self.dashboard = meraki.DashboardAPI(self.API_KEY, suppress_logging=True)

    def getOrganizations(self):
        response = self.dashboard.organizations.getOrganizations()
        return response

    def getNetworks(self, organization_id):
        response = self.dashboard.organizations.getOrganizationNetworks(
            organization_id, total_pages='all'
        )
        return response

    def getDevices(self, network_id):
        response = self.dashboard.networks.getNetworkDevices(
            network_id
        )
        return response

    def claim(self, network_id, *serials):
        response = self.dashboard.networks.claimNetworkDevices(
            network_id, serials
        )
        return response

    def blinkLeds(self, serial):
        response = self.dashboard.devices.blinkDeviceLeds(
            serial
        )
        return response

    def rebootDevice(self, serial):
        response = self.dashboard.devices.rebootDevice(
            serial
        )
        return response

    def ping(self, id, serial, target):
        response = self.dashboard.devices.createDeviceLiveToolsPing(
            serial,
            target
        )
        return response

