from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.views.generic import TemplateView

from merakiEasyWebApp.forms.cloneConfigForm import CloneConfigForm


class CloneView(LoginRequiredMixin, TemplateView):

    template_name = "merakiEasyWebApp/clone.html"
    login_url = '/login'
    redirect_field_name = 'redirect_to'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = CloneConfigForm().as_p()
        return context
