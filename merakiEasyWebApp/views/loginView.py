from django.contrib.auth import authenticate, login
from django.shortcuts import redirect
from django.views.generic import TemplateView

from merakiEasyWebApp.forms.loginForm import LoginForm


class LoginView(TemplateView):
    template_name = "merakiEasyWebApp/login.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = LoginForm().as_p()
        context['success'] = True
        return context

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        username = self.request.POST.get('user_field', None)
        password = self.request.POST.get('pass_field', None)
        user = authenticate(username=username, password=password)
        if user:
            login(request, user)
            return redirect('index')
        else:
            context['success'] = False
        return self.render_to_response(context)