from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.views.generic import TemplateView

from merakiEasyWebApp.forms.claimForm import ClaimForm


class ClaimView(LoginRequiredMixin, TemplateView):

    template_name = "merakiEasyWebApp/claim.html"
    login_url = '/login'
    redirect_field_name = 'redirect_to'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = ClaimForm().as_p()
        return context

    def post(self, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)