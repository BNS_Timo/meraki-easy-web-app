from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.views.generic import TemplateView

from merakiEasyWebApp.forms.blinkForm import BlinkForm
from merakiEasyWebApp.forms.pingForm import PingForm
from merakiEasyWebApp.forms.rebootForm import RebootForm


class TroubleView(LoginRequiredMixin, TemplateView):

    template_name = "merakiEasyWebApp/trouble.html"
    login_url = '/login'
    redirect_field_name = 'redirect_to'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['pingForm'] = PingForm().as_p()
        context['blinkForm'] = BlinkForm().as_p()
        context['rebootForm'] = RebootForm().as_p()
        return context
